import pygame
from pygame.locals import *
import match
import subprocess
import mixer
import os

import datetime
#my name is 
pygame.init()
SCREENWIDTH = 1920
SCREENHEIGHT = 1080
screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))  #Opens a window with title
#SCREENHEIGHTCENTER = SCREENHEIGHT // 2
#SCREENWIDTHCENTER = SCREENWIDTH // 2
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
backFolder = 'A_other_images'
backImage = 'litter_bkg2'
backgroundPath = os.path.join(BASE_DIR, backFolder)

pygame.display.set_caption("Litter-Bugz")
      # len -> unsure how to add background image: litter_bkg2 here 
BLACK = (0, 0, 0)
# WHITE = (255, 255, 255)
# PINK = (159, 43, 104)       #Base colors and font/font size used
# TEAL = (142, 226, 191)
# FONT = pygame.font.SysFont('Comic Sans', 72, italic = True) 

#vvAdding bouns colors for testingvv
# DARKGREEN = (26, 171, 3)
# LIGHTBLUE = (69, 236, 255)
# LIGHTGREEN = (90, 199, 40)

class makeButton():   #Class for button creation
    #def __init__(self, text, position, boxColor):  #When a menu button is created, text, screen position, and color are specified.
    def __init__(self, position, image, buttonsFolder):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        buttonPath = os.path.join(BASE_DIR, buttonsFolder)
        #pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(f'{buttonPath}/{image}.png')
        self.image = pygame.transform.scale(self.image, (500, 150))
        #self.text = text
        self.position = position
        #self.label = FONT.render(self.text, 1, DARKGREEN) #Formatting button text with pre-specified font
        #self.rect = self.label.get_rect()
        self.rect = self.image.get_rect()
        self.rect.topleft = self.position
        #self.buttonBox = pygame.Rect(self.rect.left - 10, self.rect.top - 5, self.rect.width + 20, self.rect.height + 10) #Boxes around play buttons
        #self.boxColor = boxColor


    def isClicked(self):
        return self.rect.collidepoint(pygame.mouse.get_pos())
    
#playButton = makeButton("Play", (SCREENWIDTHCENTER, SCREENHEIGHTCENTER), LIGHTBLUE) #The almighty Play button, (0, 50) (300, 100)
#playButton = makeButton("Play", (850, 350), LIGHTBLUE)
playButton = makeButton((800, 350), 'Abegin', 'final_support')
playButton2 = makeButton((800, 525), 'Dbegin', 'final_support')

#settingsButton = makeButton("Settings", (SCREENWIDTHCENTER, SCREENHEIGHTCENTER + 140), LIGHTBLUE) #optional Settings menu, slightly lower on screen, (0, 170) (300, 240)
#settingsButton = makeButton("Settings", (850, 490), LIGHTBLUE)
#settingsButton = makeButton((100, 392), 'starA', 'final_support') # len-> changed "A_dot"  "support_files" to "starA" "final_support"

#quitButton = makeButton("Quit", (SCREENWIDTHCENTER, SCREENHEIGHTCENTER + 280), LIGHTBLUE) #finally, the bottom button, Quit. (0, 290) (300, 380)
#quitButton = makeButton("Quit", (850, 630), LIGHTBLUE)
quitButton = makeButton((800, 700), 'Aend', 'final_support')

#leaderboardsButton = makeButton((100, 785), 'starD', 'final_support')


menuButtons = [playButton, playButton2, quitButton] 
running = True

while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            print("Goodbye..")
            running = False
        elif event.type == MOUSEBUTTONDOWN and event.button == 1: #Checking for clicks
            for item in menuButtons:
                if item.isClicked(): #Check if menu buttons are being clicked
                    if item == playButton: #If a menu button is clicked, the button text is evaluated and put to action
                        level_superBugz = match.Match(9, SCREENWIDTH, SCREENHEIGHT, 200) #(number of pair, background image name, SCREENWIDTH, SCREENHEIGHT, wait time)
                        level_superBugz.playGame('A_bug_images', 'A_other_images', 'sounds', 'background', 'litter', True)
                    elif item == playButton2:
                        level_infinite = match.Match(10, SCREENWIDTH, SCREENHEIGHT, 200) #(number of pair, background image name, SCREENWIDTH, SCREENHEIGHT, wait time)
                        level_infinite.playGame('D_bug_images', 'D_other_images', 'sounds', 'background', 'litter', False)
                    # elif item == settingsButton:
                    #     print("->Settings<-")
                    #     #Open Settings
                    elif item == quitButton:
                        print("Goodbye..")
                        running = False
                    # elif item == leaderboardsButton:
                    #      try:
                    #           subprocess.Popen(["notepad.exe", "leaderboards.txt"])  # Replace "notepad.exe" with the appropriate text editor for your operating system.
                    #      except FileNotFoundError:
                    #         print("Leaderboards file not found.")
    dest = (0, 0)
    backGround = pygame.image.load(f'{backgroundPath}/{backImage}.png')
    screen.blit(backGround, dest)  #Background
    for item in menuButtons: #Displaying menu buttons
        #pygame.draw.rect(screen, item.boxColor, item.buttonBox) #Boxes
        screen.blit(item.image, item.rect.topleft)
        #screen.blit(item.label, item.rect.topleft) #'buttons'

    pygame.display.update()

pygame.quit()