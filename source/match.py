#to use this class need 'import match'
#to declare an object: level1 = match.Match(3, SCREENWIDTH, SCREENHEIGHT, 1000) #(number of pair, background image name, SCREENWIDTH, SCREENHEIGHT, wait time)
#to use the function(the action game): level1.playGame('bug_images', 'other_images', 'background', 'lit09_D', True) #(bug folder name, other images folder name, background image name w/out .png, litter image name w/out.png, T/F of including superbug)

import pygame
import random
import os
import mixer
import datetime
import time
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

class Match(pygame.sprite.Sprite):

    def __init__(self, numOfPair, gameWidth, gameHeight, waitTime):
        self.numOfPair = numOfPair
        self.gameWidth = gameWidth
        self.gameHeight = gameHeight
        #if more than 10 pair, need to modify columns and rows to (columns x rows) > (number of pair x 2)
        #column CANNOT equal to rows (it's a bug not sure how to fix it)
        self.gameColumns = 5
        self.gameRows = 4
        #picSize can change how big each buz
        self.picSize = 150
        #padding is the space between each bug
        self.padding = 30
        self.leftMargin = (gameWidth - ((self.picSize + self.padding) * self.gameColumns)) // 2
        self.rightMargin = self.leftMargin
        self.topMargin = (gameHeight - ((self.picSize + self.padding) * self.gameRows)) // 2
        self.bottomMargin = self.topMargin
        self.waitTime = waitTime
        self.selection1 = None
        self.selection2 = None
        self.score = 0
        self.lastTimeElasped = 30000

    def playGame(self, bugFolder, otherFoloder, soundsFolder, backgroundImage, litterImage, needSuperbug): #->len is "otherFoloder" typo messing up anything?
        #get directory of image folder
        #PATH = os.path.join(os.path.abspath(os.getcwd()),'test_images')
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        bug_path = os.path.join(BASE_DIR, bugFolder)
        other_path = os.path.join(BASE_DIR, otherFoloder)
        sounds_path = os.path.join(BASE_DIR, soundsFolder)
        backMusic = 'Background'
        introMusic = 'Intro'
        score_font = pygame.font.SysFont('Comic Sans', 36)
        #start music

        pygame.mixer.init()
        pygame.mixer.music.load(f'{sounds_path}/{introMusic}.wav')
        pygame.mixer.music.play()
        pygame.mixer.music.unload()
        pygame.mixer.music.load(f'{sounds_path}/{backMusic}.wav')
        pygame.mixer.music.play(-1)

        #load screen
        screen = pygame.display.set_mode((self.gameWidth, self.gameHeight))

        #bgImage = pygame.image.load(f'{other_path}/{litter_bkg2}.png') # len-> nope, didn't work, I see now where to do it
        #load background image
        #need directory modify
        bgImage = pygame.image.load(f'{other_path}/{backgroundImage}.png')
        bgImage = pygame.transform.scale(bgImage, (self.gameWidth, self.gameHeight))
        bgImageRect = bgImage.get_rect()

        #get litter image
        litterPic = pygame.image.load(f'{other_path}/{litterImage}.png')
        litterPic = pygame.transform.scale(litterPic, (self.picSize, self.picSize))

        #get infinte warning message pic
        infinte_Image = pygame.image.load(f'{other_path}/modalA_04.png')
        infinte_ImageRect = infinte_Image.get_rect()

        #center
        width = self.gameWidth // 2
        height = self.gameHeight // 2

        #create list of hideBoard
        memoryPictures_All = []
        memoryPictures = []
        #load all type of image from the image file
        for i in os.listdir(bug_path):
            memoryPictures_All.append(i.split('.')[0])
        #remove all empty string
        while('' in memoryPictures_All):
            memoryPictures_All.remove('')

        memoryPictures = memoryPictures_All.copy()
        #minus some part of the pics to get target number of pair
        
        target = len(memoryPictures) - self.numOfPair
        for i in range(target):
            memoryPictures.pop(0)

        #get everything in pair and shuffle
        memoryPicturePair = memoryPictures.copy()
        memoryPictures.extend(memoryPicturePair)
        memoryPicturePair.clear()

        #load supper bug to match
        if needSuperbug == True:
            memoryPictures.append('Bomb')
            memoryPictures.append('monarch')

        random.shuffle(memoryPictures)

        #load images into memory
        #hideboard used to keep track of if that index had been selected
        memPics = []
        rectangleImage = []
        hideBoard = []
        litterDis = []
        litterDisplay = []
        for i in memoryPictures:
            #if exists in the bugz file, load the image from there
            if os.path.exists(f'{bug_path}/{i}.png') == True:
                pic = pygame.image.load(f'{bug_path}/{i}.png')
                pic = pygame.transform.scale(pic, (self.picSize, self.picSize))
                memPics.append(pic)
                retanglePic = pic.get_rect()
                rectangleImage.append(retanglePic)
                litterDis.append(litterPic)
                litterDisplay.append(retanglePic)
            else:
                supperPic = pygame.image.load(f'{other_path}/{i}.png')
                supperPic = pygame.transform.scale(supperPic, (self.picSize, self.picSize))
                memPics.append(supperPic)
                retanglePic_supper = supperPic.get_rect()
                rectangleImage.append(retanglePic_supper)
                litterDis.append(litterPic)
                litterDisplay.append(retanglePic_supper)


        #load to hidden board; create the size for each index
        #create position
        for i in range (len(rectangleImage)):
            rectangleImage[i][0] = self.leftMargin + ((self.picSize + self.padding) * (i % self.gameColumns))
            rectangleImage[i][1] = self.topMargin + ((self.picSize + self.padding) * (i % self.gameRows))
            litterDisplay[i][0] = self.leftMargin + ((self.picSize + self.padding) * (i % self.gameColumns))
            litterDisplay[i][1] = self.topMargin + ((self.picSize + self.padding) * (i % self.gameRows))
            hideBoard.append(False)
            
        #game loop
        gameLoop = True
        while gameLoop:
            screen.blit(bgImage, bgImageRect)
            score_text = score_font.render("Score: " + str(self.score), True, BLACK)
            screen.blit(score_text, (20, 20))
            for event in pygame.event.get():
                #if select quit, quit game
                if event.type == pygame.QUIT:
                    pygame.mixer.music.stop()
                    self.saveScore(self.score)
                    gameLoop = False
                #if click on certain index...
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    for i in rectangleImage:
                        if i.collidepoint(event.pos):
                            if hideBoard[rectangleImage.index(i)] != True:
                                if self.selection1 != None:
                                    self.selection2 = rectangleImage.index(i)
                                    hideBoard[self.selection2] = True
                                else:
                                    self.selection1 = rectangleImage.index(i)
                                    hideBoard[self.selection1] = True
    
            #display all bug and litter
            for i in range(len(memoryPictures)):
                if hideBoard[i] == True:
                    screen.blit(memPics[i], rectangleImage[i])
                else:
                    screen.blit(litterDis[i], litterDisplay[i])
            pygame.display.update()

            #super bugz feature
            startTime = pygame.time.get_ticks()
            if (self.selection1 != None and memoryPictures[self.selection1] == "Bomb") or (self.selection2 != None and memoryPictures[self.selection2] == "Bomb"):
                print ("Boom!")
                while pygame.time.get_ticks() < startTime + 1000:
                    bombMessage = score_font.render("Bomb! Game Over ", True, BLACK)
                    screen.blit(bombMessage, (width,height))
                    pygame.display.update()
                pygame.mixer.music.stop()
                self.saveScore(self.score)
                gameLoop = False
            if (self.selection1 != None and memoryPictures[self.selection1] == "monarch"):
                self.score += 20
                while pygame.time.get_ticks() < startTime + 1000:
                    bombMessage = score_font.render("Happy! +20 ", True, BLACK)
                    screen.blit(bombMessage, (width,height))
                    pygame.display.update()
                print ("happy!")
                self.selection1 = None
            if (self.selection2 != None and memoryPictures[self.selection2] == "monarch"):
                self.score += 20
                while pygame.time.get_ticks() < startTime + 1000:
                    bombMessage = score_font.render("Happy! +20 ", True, BLACK)
                    screen.blit(bombMessage, (width,height))
                    pygame.display.update()
                print ("happy!")
                self.selection2 = None

            #check if they match, if matched change the hideboard
            if self.selection1 != None and self.selection2 != None:
                if memoryPictures[self.selection1] == memoryPictures[self.selection2]:
                    self.score += 1  # Increase the score
                    self.selection1, self.selection2 = None, None
                else:
                    pygame.time.wait(self.waitTime)
                    hideBoard[self.selection1] = False
                    hideBoard[self.selection2] = False
                    self.selection1, self.selection2 = None, None

            #infinite mode
            timeElapsedNew = pygame.time.get_ticks()
            infinite_memoryPictures = memoryPictures_All.copy()
            if needSuperbug == False and timeElapsedNew > self.lastTimeElasped:
                #warning message
                while pygame.time.get_ticks() < timeElapsedNew + 3000:
                    screen.blit(infinte_Image, infinte_ImageRect.center)
                    pygame.display.update()

                #if only select one at the time, change that one back to false, won't update
                if self.selection1 != None:
                    hideBoard[self.selection1] = False
                elif self.selection2 != None:
                    hideBoard[self.selection2] = False

                new_bug_Count = hideBoard.count(True)
                #update to new bug in hidden board
                random.shuffle(infinite_memoryPictures)
                target_new = len(infinite_memoryPictures) - (new_bug_Count // 2)
                for i in range(target_new):
                    infinite_memoryPictures.pop(0)

                memoryPicturePair = infinite_memoryPictures.copy()
                infinite_memoryPictures.extend(memoryPicturePair)
                memoryPicturePair.clear()

                #load new bug to position
                for i in range(len(hideBoard)):
                    if hideBoard[i] == True:
                        hideBoard[i] = False
                        memoryPictures[i] = infinite_memoryPictures[0]
                        pic_inf = pygame.image.load(f'{bug_path}/{infinite_memoryPictures.pop(0)}.png')
                        pic_inf = pygame.transform.scale(pic_inf, (self.picSize, self.picSize))
                        memPics[i] = pic_inf
                        retanglePic = pic_inf.get_rect()
                        rectangleImage[i] = retanglePic
                        rectangleImage[i][0] = self.leftMargin + ((self.picSize + self.padding) * (i % self.gameColumns))
                        rectangleImage[i][1] = self.topMargin + ((self.picSize + self.padding) * (i % self.gameRows))

                self.lastTimeElasped = timeElapsedNew + 30000
                
            #if everything in hideboard are true, end game
            #hideBoard score, true = 1, false = 0
            win = 1
            for score in range(len(hideBoard)):
                win *= hideBoard[score]

            if win == 1:
                pygame.mixer.music.stop()
                gameLoop = False
                self.saveScore(self.score)


            pygame.display.update()

    def saveScore(self, score):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open("leaderboards.txt", "a") as file:
            file.write(f"Score: {score} - Date: {timestamp}\n")