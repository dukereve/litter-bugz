
import random
import string
import math
import numpy as np
import time

def createNewSet (input):
    #generate given number pairs of numbers
    #pairs = [i for i in range(input)] * 2
    pairs = [i for i in string.ascii_lowercase[:input]] * 2
    random.shuffle(pairs)

    #shownBoard is what the player will see, hiddenBoard is hidden from player
    shownBoard = [i+1 for i in range (input * 2)]
    hiddenBoard = [['']for i in range (input * 2)]

    shownBoard = np.array(shownBoard).astype(str)

    #assign all pairs to the hidden board
    for position in range(input * 2):
            hiddenBoard[position] = pairs.pop()

    row, col = getRowCol(input)
    row = int(row)
    col = int(col)
    shownBoard1 = np.array(shownBoard).reshape(row, col)
    hiddenBoard1 = np.array(hiddenBoard).reshape(row,col)


    return(shownBoard1, hiddenBoard1)

#check if two guesses are match
def check (hiddenBoard, shownBoard, guess1, guess2):
     correct = True
     guess1_row, guess1_col = np.where(shownBoard == guess1)
     guess2_row, guess2_col = np.where(shownBoard == guess2)

     if len(guess1_row) == 0 or len(guess2_row) == 0 :
          print('Already matched.')
          correct = False
     else:
          guess1_row = int(guess1_row)
          guess1_col = int(guess1_col)
          guess2_row = int(guess2_row)
          guess2_col = int(guess2_col)

          if hiddenBoard[guess1_row][guess1_col] == hiddenBoard[guess2_row][guess2_col]:
               print("You matched!")
               correct = True
          else:
               print("Try again.")
          #show hidden value of that position
               print(int(guess1) + 1, ': ', hiddenBoard[guess1_row][guess1_col])
               print(int(guess2) + 1, ': ', hiddenBoard[guess2_row][guess2_col])
               correct = False
     return correct

def inputPosition(hideBoard, shownBoard):
     #get user inputs and check if they match
     userInput1 = input('First position: ')
     userInput2 = input('Second position: ')
     user1 = int(userInput1)
     user2 = int(userInput2)

     if user1 >= ((len(hideBoard)*len(hideBoard[0]))+1) or user2 >= ((len(hideBoard)*len(hideBoard[0]))+1) or user1 == user2:
          print('Invalid Position, try again.')
          correct = False
     else:
          correct = check(hideBoard, shownBoard, userInput1, userInput2)
     return correct, userInput1, userInput2

#display correct matched
def updateHide(input1, input2, hideBoard, displayBoard, countDown):
     guess1_row, guess1_col = np.where(displayBoard == input1)
     guess2_row, guess2_col = np.where(displayBoard == input2)
     guess1_row = int(guess1_row)
     guess1_col = int(guess1_col)
     guess2_row = int(guess2_row)
     guess2_col = int(guess2_col)

     #change displayBoard to show certain pos in hideBoard?
     displayBoard[guess1_row][guess1_col] = hideBoard[guess1_row][guess1_col]
     displayBoard[guess2_row][guess2_col] = hideBoard[guess2_row][guess2_col]
     countDown += 1
     return displayBoard, countDown

#get how many rows and cols needed
#get squareroot first then see what's the closest pair of numbers that matche
def getRowCol(input):
     temp = input * 2
     square = math.sqrt(temp) // 1
     print(square)
     square2 = square
     while square2 < temp:
          check = square * square2

          if check == temp:
               return square, square2
          
          square2 = square2 + 1

def actual_game(number_matches):
     #create new set
     #guess is what user will see, hide is hidden; guessed is number of pairs user matched; correctList stores positions that are matched.
     guess, hide = createNewSet(number_matches)
     guessed = 0

     #start timing
     startTime = time.time()

     #while there are still unmatched, continue to get input from user and check if they match
     #is_correct is whether the inputs are match (T/F); user1 and user2 are user inputs; correctedList is a list to store all matched position
     while guessed != number_matches:
          print(guess)
          is_correct, user1, user2= inputPosition(hide, guess)
          #if it is correct, show the hide board value of that position
          if is_correct == True:
               guess, guessed = updateHide(user1, user2, hide, guess, guessed)

     #calculate the time used in this game
     endTime = time.time()
     play_time = round(endTime - startTime, 2)
     print(play_time, 'sec')

actual_game(2)