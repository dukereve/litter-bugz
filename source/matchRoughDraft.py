
import pygame
import random
import os

pygame.init()

#set the size of the screen
#columns CANNOT equal to rows
gameWidth = 840
gameHeight = 640
gameColumns = 2
gameRows = 3
picSize = 128
padding = 10
leftMargin = (gameWidth - ((picSize + padding) * gameColumns)) // 2
rightMargin = leftMargin
topMargin = (gameHeight - ((picSize + padding) * gameRows)) // 2
bottomMargin = topMargin
selection1 = None
selection2 = None
white = (255, 255, 255)
black = (0, 0, 0)


testLength = 10

#get directory of image folder
#PATH = os.path.join(os.path.abspath(os.getcwd()),'test_images')
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
docs_path = os.path.join(BASE_DIR, "bug_images")
other_path = os.path.join(BASE_DIR, "other_images")
#load screen
screen = pygame.display.set_mode((gameWidth, gameHeight))
pygame.display.set_caption('Memory Game Test')

#load background image
#need directory modify
bgImage = pygame.image.load(f'{other_path}/background.png')
bgImage = pygame.transform.scale(bgImage, (gameWidth, gameHeight))
bgImageRect = bgImage.get_rect()

#create list of hideBoard
memoryPictures = []
#load all type of image from the image file
#all image of bug shoud name as "{color}.png"
for i in os.listdir(docs_path):
     memoryPictures.append(i.split('.')[0])
#remove all empty string
while('' in memoryPictures):
     memoryPictures.remove('')

# print(memoryPictures)
target = len(memoryPictures) - testLength
#print(memoryPictures.pop(target))
for i in range(target):
     memoryPictures.pop(0)


#get everything in pair and shuffle
memoryPicturePair = memoryPictures.copy()
memoryPictures.extend(memoryPicturePair)
memoryPicturePair.clear()
memoryPictures.append('superA_01')
random.shuffle(memoryPictures)

#load images into memory
#hideboard used to keep track of if that index had been selected
memPics = []
rectangleImage = []
hideBoard = []
for i in memoryPictures:
    if os.path.exists(f'{docs_path}/{i}.png') == True:
        pic = pygame.image.load(f'{docs_path}/{i}.png')
        pic = pygame.transform.scale(pic, (picSize, picSize))
        memPics.append(pic)
        retanglePic = pic.get_rect()
        rectangleImage.append(retanglePic)
    else:
        #maybe use a folder to load all super bugz?
        supperPic = pygame.image.load(f'{other_path}/superA_01.png')
        supperPic = pygame.transform.scale(supperPic, (picSize, picSize))
        memPics.append(supperPic)
        retanglePic_supper = supperPic.get_rect()
        rectangleImage.append(retanglePic_supper)

#load to hidden board; create the size for each index
#create position
for i in range (len(rectangleImage)):
     rectangleImage[i][0] = leftMargin + ((picSize + padding) * (i % gameColumns))
     rectangleImage[i][1] = topMargin + ((picSize + padding) * (i % gameRows))
     hideBoard.append(False)

# #game loop
gameLoop = True
while gameLoop:
    screen.blit(bgImage, bgImageRect)

    for event in pygame.event.get():
        #if select quit, quit game
        if event.type == pygame.QUIT:
            gameLoop = False
        #if click on certain index...
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            for i in rectangleImage:
                if i.collidepoint(event.pos):
                    if hideBoard[rectangleImage.index(i)] != True:
                        if selection1 != None:
                            selection2 = rectangleImage.index(i)
                            hideBoard[selection2] = True
                        else:
                            selection1 = rectangleImage.index(i)
                            hideBoard[selection1] = True
    
    #display all white boxes
    for i in range(len(memoryPictures)):
        if hideBoard[i] == True:
            screen.blit(memPics[i], rectangleImage[i])
        else:
            pygame.draw.rect(screen, white, (rectangleImage[i][0], rectangleImage[i][1], picSize, picSize))
    pygame.display.update()


    #check if they match, if matched change the hideboard
    if selection1 != None and selection2 != None:
        if memoryPictures[selection1] == memoryPictures[selection2]:
            selection1, selection2 = None, None
        #--------------super bug feature----------------------#
                #elif memoryPictures[selection1] = ...
                #hideBoard[self.selection1] = True
        else:
            pygame.time.wait(1000)
            hideBoard[selection1] = False
            hideBoard[selection2] = False
            selection1, selection2 = None, None

    #if everything in hideboard are true, end game
    #hideBoard score, true = 1, false = 0
    win = 1
    for score in range(len(hideBoard)):
        win *= hideBoard[score]

    if win == 1:
        gameLoop = False

    pygame.display.update()


pygame.quit()

import pygame
import random
import os

pygame.init()

#set the size of the screen
#columns CANNOT equal to rows
gameWidth = 840
gameHeight = 640
gameColumns = 2
gameRows = 3
picSize = 128
padding = 10
leftMargin = (gameWidth - ((picSize + padding) * gameColumns)) // 2
rightMargin = leftMargin
topMargin = (gameHeight - ((picSize + padding) * gameRows)) // 2
bottomMargin = topMargin
selection1 = None
selection2 = None
white = (255, 255, 255)
black = (0, 0, 0)

def good_bug ():
    # body of the function
    print ("happy!")
    return
#defined bad bug
def bad_bug ():
    # body of the function
    print ("Boom!")

#get directory of image folder
#PATH = os.path.join(os.path.abspath(os.getcwd()),'test_images')
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
docs_path = os.path.join(BASE_DIR, "bug_images")
other_path = os.path.join(BASE_DIR, "other_images")
#load screen
screen = pygame.display.set_mode((gameWidth, gameHeight))
pygame.display.set_caption('Memory Game Test')

#load background image
#need directory modify
bgImage = pygame.image.load(f'{other_path}/background.png')
bgImage = pygame.transform.scale(bgImage, (gameWidth, gameHeight))
bgImageRect = bgImage.get_rect()

#create list of hideBoard
memoryPictures = []
#load all type of image from the image file
#all image of bug shoud name as "{color}.png"
for i in os.listdir(docs_path):
     memoryPictures.append(i.split('.')[0])
#remove all empty string
while('' in memoryPictures):
     memoryPictures.remove('')

# print(memoryPictures)
testLength=2
target = len(memoryPictures) - testLength
#print(memoryPictures.pop(target))
for i in range(target):
     memoryPictures.pop(0)


#get everything in pair and shuffle
memoryPicturePair = memoryPictures.copy()
memoryPictures.extend(memoryPicturePair)
memoryPicturePair.clear()
memoryPictures.append('superA_01')
random.shuffle(memoryPictures)
print (memoryPictures)
#load images into memory
#hideboard used to keep track of if that index had been selected
memPics = []
rectangleImage = []
hideBoard = []
for i in memoryPictures:
    if os.path.exists(f'{docs_path}/{i}.png') == True:
        pic = pygame.image.load(f'{docs_path}/{i}.png')
        pic = pygame.transform.scale(pic, (picSize, picSize))
        memPics.append(pic)
        retanglePic = pic.get_rect()
        rectangleImage.append(retanglePic)
    else:
        #maybe use a folder to load all super bugz?
        supperPic = pygame.image.load(f'{other_path}/superA_01.png')
        supperPic = pygame.transform.scale(supperPic, (picSize, picSize))
        memPics.append(supperPic)
        retanglePic_supper = supperPic.get_rect()
        rectangleImage.append(retanglePic_supper)

#load to hidden board; create the size for each index
#create position
for i in range (len(rectangleImage)):
     rectangleImage[i][0] = leftMargin + ((picSize + padding) * (i % gameColumns))
     rectangleImage[i][1] = topMargin + ((picSize + padding) * (i % gameRows))
     hideBoard.append(False)

# #game loop
gameLoop = True
while gameLoop:
    screen.blit(bgImage, bgImageRect)

    for event in pygame.event.get():
        #if select quit, quit game
        if event.type == pygame.QUIT:
            gameLoop = False
        #if click on certain index...
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            for i in rectangleImage:
                if i.collidepoint(event.pos):
                    if hideBoard[rectangleImage.index(i)] != True:
                        if selection1 != None:
                            selection2 = rectangleImage.index(i)
                            hideBoard[selection2] = True
                        else:
                            selection1 = rectangleImage.index(i)
                            hideBoard[selection1] = True
    
    #display all white boxes
    for i in range(len(memoryPictures)):
        if hideBoard[i] == True:
            screen.blit(memPics[i], rectangleImage[i])
        else:
            pygame.draw.rect(screen, white, (rectangleImage[i][0], rectangleImage[i][1], picSize, picSize))
    pygame.display.update()

    #check if they match, if matched change the hideboard
    if selection1 != None and memoryPictures[selection1] == "Bomb":
        print ("Boom!")
        gameLoop = False
        #
    if selection1 != None and memoryPictures[selection1] == "monarch":
        good_bug()
        continue
        #print ("happy!")
    if selection1 != None and selection2 != None:
        if memoryPictures[selection1] == memoryPictures[selection2]:
            selection1, selection2 = None, None
        #--------------super bug feature----------------------#
                #elif memoryPictures[selection1] = ...
                #hideBoard[self.selection1] = True
        else:
            pygame.time.wait(1000)
            hideBoard[selection1] = False
            hideBoard[selection2] = False
            selection1, selection2 = None, None

    #if everything in hideboard are true, end game
    #hideBoard score, true = 1, false = 0
    win = 1
    for score in range(len(hideBoard)):
        win *= hideBoard[score]

    if win == 1:
        gameLoop = False

    pygame.display.update()


pygame.quit()

#if 
pygame.quit()
