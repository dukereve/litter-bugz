import pygame
from pygame.locals import *
import match
import subprocess
import mixer
import os

#len -> level will be 6x6 grid using new levelA files (sizes corrected/backgrounds fixed)
pygame.init()
SCREENWIDTH = 800
SCREENHEIGHT = 1000
screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))  #Opens a window with title

pygame.display.set_caption("Litter-Bugz")

BLACK = (0, 0, 0)
#len -> need to add level A background

class makeButton():   #len -> insert actual level A buttons ? change name to Button?
    def __init__(self, position, image, buttonsFolder):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        buttonPath = os.path.join(BASE_DIR, buttonsFolder)

        self.image = pygame.image.load(f'{buttonPath}/{image}.png')

        self.position = position

        self.rect = self.image.get_rect()
        self.rect.topleft = self.position

    def isClicked(self):
        return self.rect.collidepoint(pygame.mouse.get_pos())
    
playButton = makeButton((690, 30), 'A_begin', 'levelAsupport')

settingsButton = makeButton((815, 200), 'A_dot', 'levelAsupport')


quitButton = makeButton((770, 400), 'A_end', 'levelAsupport') #len -> use actual button

leaderboardsButton = makeButton((650, 530), 'A_star', 'levelAsupport') #len -> remove leaderboards from level A 


menuButtons = [playButton, settingsButton, quitButton] 
menuButtons.append(leaderboardsButton) #len -> remove this from level A
running = True

while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            print("Goodbye..")
            running = False
        elif event.type == MOUSEBUTTONDOWN and event.button == 1: #Checking for clicks
            for item in menuButtons:
                if item.isClicked(): #Check if menu buttons are being clicked
                    if item == playButton: #If a menu button is clicked, the button text is evaluated and put to action
                        level1 = match.Match(8, SCREENWIDTH, SCREENHEIGHT, 200) #(number of pair, background image name, SCREENWIDTH, SCREENHEIGHT, wait time)
                        level1.playGame('bug_images', 'other_images', 'sounds', 'background', 'lit09_D', True)
                    elif item == settingsButton:
                        print("->Settings<-")
                        #Open Settings
                    elif item == quitButton:
                        print("Goodbye..")
                        running = False
                    elif item == leaderboardsButton:
                         try:
                              subprocess.Popen(["notepad.exe", "leaderboards.txt"])  # Replace "notepad.exe" with the appropriate text editor for your operating system.
                         except FileNotFoundError:
                            print("Leaderboards file not found.")


    screen.fill(BLACK)  #Background
    for item in menuButtons: #Displaying menu buttons
        #pygame.draw.rect(screen, item.boxColor, item.buttonBox) #Boxes
        screen.blit(item.image, item.rect.topleft)
        #screen.blit(item.label, item.rect.topleft) #'buttons'

    pygame.display.update()

pygame.quit()
